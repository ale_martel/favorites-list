app.controller('dashboardCtrl', function($scope, $http, user, $route){
	$scope.user = user.getName();
	$http({
		url: 'http://localhost:8000/articles',
		method: 'GET'
	}).then(function (response) {
		$scope.articles = response.data;
	}, function (error) {
		console.log(error);
	});
	
	$scope.sort = function(keyname){
		$scope.sortKey = keyname;
		$scope.reverse = !$scope.reverse;
	}
	
	$scope.addToFavorites = function(id){
		console.log("addFav" + id);
		var favoriteRequest = {
			id: 1,
			article_id: id
		}
		$http({
			url: 'http://localhost:8000/favorites',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(favoriteRequest)
		}).then(function (response) {
			
		}, function (error) {
			console.log(error);
		});
	}

	$scope.logout = function(){
		user.userLoggedOut();
		$route.reload();
	}
})