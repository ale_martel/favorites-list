app.controller('loginCtrl', function($scope, $http, $location, user){
	$scope.login = function() {
		var loginRequest = {
			name: $scope.username,
			password: $scope.password
		}
		console.log(JSON.stringify(loginRequest));
		$http({
			url: 'http://localhost:8000/user',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(loginRequest)
		}).then(function (response) {
		   if(response.data.length > 0){
			user.userLoggedIn();
			user.setName(loginRequest.name);
			$location.path('/dashboard');
		   }
		}, function (error) {
			console.log(error);
		});
	}

	$scope.goHome = function(){
		$location.path('/');
	}
})