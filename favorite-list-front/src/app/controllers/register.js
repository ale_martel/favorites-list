app.controller('registerCtrl', function($scope, $http, $location, user){
	$scope.register = function() {
		var registerRequest = {
			name: $scope.username,
			password: $scope.password
		}
		console.log(registerRequest);
		if( /(^[A-Za-z])([a-zA-Z0-9_])+$/.test(registerRequest.name) && /([a-zA-Z0-9_-\s])+$/.test(registerRequest.password)){
			$http({
				url: 'http://localhost:8000/users',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				data: JSON.stringify(registerRequest)
			}).then(function (response) {
				user.userLoggedIn();
				user.setName(response.data.name);
				$location.path('/dashboard');
			}, function (error) {
				console.log(error);
			});
		}else{
			console.log("invalid");
		}
	}

	$scope.goHome = function(){
		$location.path('/');
	}
})