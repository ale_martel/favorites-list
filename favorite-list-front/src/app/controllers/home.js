app.controller('homeCtrl', function($scope, $location, $translate){
	$scope.goToLogin = function(){
		$location.path('/login');
	};
	$scope.goToRegister = function(){
		$location.path('/register');
	}

	$scope.lang = function(trans){
		$translate.use(trans);
	}
})