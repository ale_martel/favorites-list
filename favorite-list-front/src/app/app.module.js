var app = angular.module('main', ['ngRoute', 'angularUtils.directives.dirPagination', 'pascalprecht.translate']);

app.config(function($translateProvider){
	$translateProvider.translations('en', {
		Home: "Home",
        Login: "Login",
        Register: "Register",
        Username: "Username",
        Password: "Password",
        Name: "Name",
        Price: "Price",
        Favorite: "Favorite",
        Search: "Search"
	});
	$translateProvider.translations('es', {
		Home: "Inicio",
        Login: "Iniciar sesión",
        Register: "Registro",
        Username: "Nombre de usuario",
        Password: "Contraseña",
        Name: "Nombre",
        Price: "Precio",
        Favorite: "Favorito",
        Search: "Buscar"
	});
	$translateProvider.preferredLanguage('en');
});

app.service('user', function(){
	this.setName = function(name){
		localStorage.setItem('username', name);
	};
	this.getName = function(){
		return localStorage.getItem("username");
	};
	this.userLoggedIn = function(){
		localStorage.setItem('loggedin', true);
	};
	this.userLoggedOut = function(){
		localStorage.setItem('loggedin', false);
	};
	this.isUserLoggedIn = function(){
		return localStorage.getItem('loggedin');
	};
})