app.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: './src/app/components/home.html',
		controller: 'homeCtrl'
	})
	.when('/login', {
		templateUrl: './src/app/components/login.html',
		controller: 'loginCtrl'
	})
	.when('/register', {
		templateUrl: './src/app/components/register.html',
		controller: 'registerCtrl'
	})
	.when('/dashboard', {
		resolve: {
			check: function($location, user){
				if(user.isUserLoggedIn()=="false"){
					$location.path('/login');
				}
			}
		},
		templateUrl: './src/app/components/dashboard.html',
		controller: 'dashboardCtrl'
	})
	.otherwise({
		template: '404'
	})
});