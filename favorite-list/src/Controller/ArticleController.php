<?php
namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ArticleController extends ApiController
{
    /**
     * @Route("/articles")
     * @Method("GET")
     */
    public function articlesAction(ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->transformAll();
        return $this -> respond($articles);
    }
}

