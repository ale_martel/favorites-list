<?php
namespace App\Controller;

use App\Entity\Favorite;
use App\Entity\Article;
use App\Repository\FavoriteRepository;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class FavoriteController extends ApiController
{

    

    /**
     * @Route("/favorites", methods={"GET"})
     */
    public function favoritesAction(Request $request, FavoriteRepository $favoriteRepository, EntityManagerInterface $em)
    {
        $favorites = $favoriteRepository->transformAll();
        return $this -> respond($favorites);
    }

    /**
     * @Route("/favorites", methods={"POST"})
     */
    public function create(Request $request, FavoriteRepository $favoriteRepository, EntityManagerInterface $em, ArticleRepository $articleRepository)
    {
        $request = $this->transformJsonBody($request);

        if(! $request){
            return $this->respondValidationError('Please provide a valid article id');
        }
        $favorite = $favoriteRepository->findById($request->get('id'));
        if ($favorite == null){
            $favorite = new Favorite;
        }
        $article = $articleRepository->findById($request->get('article_id'));
        $favorite->addArticle($article);
        $em->persist($favorite);
        $em->flush();

        return $this->respondCreated($favoriteRepository->transform($favorite));
    }

    

    
}