<?php
namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class UserController extends ApiController
{
    /**
     * @Route("/users")
     * @Method("POST")
     */
    public function create(Request $request, UserRepository $userRepository, EntityManagerInterface $em)
    {
        $request = $this->transformJsonBody($request);

        if(! $request){
            return $this->respondValidationError('Please provide a valid User');
        }

        $user = new User;
        $user->setName($request->get('name'));
        $user->setPassword($request->get('password'));
        $em->persist($user);
        $em->flush();

        return $this->respondCreated($userRepository->transform($user));
    }

    /**
     * @Route("/user")
     * @Method("POST")
     */
    public function articlesAction(Request $request, UserRepository $userRepository)
    {
        $request = $this->transformJsonBody($request);
        $users = $userRepository->findByNameAndPasswordField($request->get('name'), $request->get('password'));
        return $this -> respond($users);
    }
}